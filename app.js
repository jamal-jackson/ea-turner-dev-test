// Server Setup
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var db = require('./lib/db');
var Movies = require('./models/Movies.js')

var app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// Routes

app.get('/', function(req, res){
        console.log("request handler for start called");
        res.render('index', {title: 'EA TNN NodeJS Test'});
});

app.get('/find',function(req, res){
	var search = req.body.title;
	Movies.findTitles(search, function(err, user) {
        if (err) throw err;
        console.log("request handler for start called");
    	res.render('results', {titleInput: req.body.titleInput});
    });
})
//Port Lietening
app.listen(3000);
