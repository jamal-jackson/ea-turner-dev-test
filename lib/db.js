/** DB JS file created for express app **/

var mongoose = require('mongoose');
mongoose.connect('mongodb://readuser:reader1234@SG-mssmongodev02-874.servers.mongodirector.com:27017/dev-test');
mongoose.connection.on("open", function(){
  console.log("mongodb is connected!!");
// List Connection names to find collection to search for title
  mongoose.connection.db.collectionNames(function (err, names) {
        console.log(names);
        module.exports.Collection = names;
    });
});
module.exports.mongoose = mongoose;
module.exports.Schema = Schema;

function find (collec, query, callback) {
    mongoose.connection.db.collection(collec, function (err, collection) {
    collection.find(query).toArray(callback);
    });
}